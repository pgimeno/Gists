-- This file is donated to the public domain.
-- Original author: Pedro Gimeno Fortea.
-- Many thanks to Paramat for the numerous tips.

minetest.set_mapgen_setting('mg_name', 'flat', true)
minetest.set_mapgen_setting('mgflat_ground_level', 0, true)
-- water has to be below ground level to not inundate everything;
-- it can't be equal lest you spawn at (0,0,0). It should be equal to
-- ground_level - 1 for correct lighting underground, I'm told.
minetest.set_mapgen_setting('water_level', -1, true)
minetest.set_mapgen_setting('mg_flags', 'light, nocaves, nodungeons, nodecorations', true);

minetest.clear_registered_biomes()
minetest.clear_registered_ores()
minetest.clear_registered_decorations()

minetest.register_biome {
  name = "flatworld",
  --node_dust = "",
  node_top = "default:dirt_with_grass", -- layer 1
  depth_top = 1,
  node_filler = "default:dirt", -- layer 2
  depth_filler = 1,
  node_stone = "default:dirt", -- layer 3 (all the way to the bottom)
  --node_water_top = "",
  --depth_water_top = ,
  --node_water = "",
  --node_river_water = "",
  y_min = -31000,
  y_max = 31000,
  heat_point = 50,
  humidity_point = 50,
}

-- <paramat> you can stack biomes on top of each other if you need changes of
--           materials with depth

-- Note: On 0.4.15 it generates some errors but these are safe to ignore. I'm
-- told they have been removed from current master.